const dotenv = require("dotenv");
const express = require("express");
const multer = require("multer");
const bcrypt = require("bcrypt");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

const routers = require("./source/routers/index");
const session = require("express-session");
const { MemoryStore } = require("express-session");


const app = express();
const sessionStorage = new MemoryStore
dotenv.config()

// bcrypt config //
const salt = process.env.SALT_ROUNDS
//  end of bcrypt config //

// Multer config //
const fileStorage = multer.diskStorage({
  destination: (req, res, callback) => 
  {
    callback(null, 'images')
  },

  filename: (req, file, callback) => {
    callback(null, new Date().getTime() + `-` + file.originalname)
  }
})

const fileFilter = (req, file, callback) => {
  if(file.mimetype === 'image/png' || 
  file.mimetype === 'image/jpg' || 
  file.mimetype === 'image/jpeg')
  {
    callback(null, true)
  } else {
    callback(null, false)
  }
}
//  End multer config //

app.use(express.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
app.use(express.json())
app.use(multer({storage: fileStorage, fileFilter: fileFilter}).single('image'));
app.use(bodyParser.json())
app.use(cookieParser())

// app.use(session(
//   {
//     secret: 'key_session_sementara',
//     name: 'user_session',
//     saveUninitialized: true,
//     store: sessionStorage,
//     resave: false,
//     rolling: true,
//     cookie: {
//       secure: false,
//       maxAge: 1000000 * 60,
//       httpOnly: true
//     }
//   }
// ))

app.use("/users", routers.r_user)
app.use("/movies", routers.r_movie)
app.use("/rent", routers.r_rent)
app.use("/transaksi", routers.r_transaksi)
app.use("/return", routers.r_return)
app.use("/review", routers.r_review)
app.use("/alert", routers.r_alert)
app.use("/", (req, res) => {res.json({pesan: `halaman awal`})})

app.listen(process.env.APP_PORT, console.info(`Server berjalan di ${process.env.APP_HOST}:${process.env.APP_PORT}`))