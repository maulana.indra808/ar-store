const express = require("express");
const controllers = require("../controllers");

let r_alert = express.Router()

r_alert.get("/", (req, res) => { res.json({ pesan: `ini pesan dari alert`} )})
r_alert.post("/", controllers.c_alert.kirimAlert)

module.exports = r_alert