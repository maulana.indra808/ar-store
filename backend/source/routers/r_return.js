const express = require("express");
const controllers = require("../controllers");

const r_return = express.Router();

r_return.get("/", (req, res) => { res.json({ pesan: `Ini pesan dari return` }) })
r_return.post("/", controllers.c_return.pengembalianBarang) // pengembalian pesanan

module.exports = r_return