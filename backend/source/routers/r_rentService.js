const express = require("express");
const controllers = require("../controllers");

const r_rent = express.Router();

r_rent.get("/", (req, res) => {res.status(200).json({ pesan: `Pesan dari touter rent service` })})

r_rent.post("/", controllers.c_rent.insertPesanan)
r_rent.post("/faktur", controllers.c_rent.insertFaktur)

module.exports = r_rent