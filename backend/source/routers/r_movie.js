const express = require("express");
const controllers = require("../controllers");

const r_movie = express.Router();


r_movie.get("/", (req, res) => {res.json({ pesan: `Ini pesan router moview` }) })

r_movie.post("/", controllers.c_movies.insertMovie)

r_movie.put("/", controllers.c_movies.updateMovie)

module.exports = r_movie