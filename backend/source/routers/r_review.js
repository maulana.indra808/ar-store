const express = require("express");
const controllers = require("../controllers");

const r_review = express.Router();

r_review.get("/", (req, res) => { res.status(200).json({pesan: `Ini pesan dari review`})} )

r_review.post("/", controllers.c_review.insertReview)
r_review.delete("/", controllers.c_review.deleteReview)

module.exports = r_review