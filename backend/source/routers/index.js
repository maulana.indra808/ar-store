const r_user = require("./r_user")
const r_movie = require("./r_movie")
const r_rent = require("./r_rentService")
const r_transaksi = require("./r_transaksi")
const r_return = require("./r_return")
const r_review = require("./r_review")
const r_alert = require("./r_alert")
const verify_token = require("../middlewares/verify_token")
const routers = {
  verify_token,
  r_user,
  r_movie,
  r_rent,
  r_transaksi,
  r_return,
  r_review,
  r_alert
}

module.exports = routers