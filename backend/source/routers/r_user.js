const express = require("express");
const controllers = require("../controllers/index")
const verify_token = require("../middlewares/verify_token")


const r_user = express.Router();

// Register User
r_user.get("/session-test", (req, res) => {res.json({user_data: req.session.user, email: req.session.email})})
r_user.get("/register", (req, res) => {res.json({pesan: `ini pesan dari router`})})
r_user.get("/logout", (req, res) => { req.session.destroy(); res.status(200).json({pesan: "Terimakasih, sampai bertemu lagi"}) })
r_user.get("/profile", verify_token, controllers.c_users.lihatSemuaProfiile)

r_user.post("/status-pinjaman", controllers.c_users.cekStatusPinjaman) // melihat status pinjaman user
r_user.post("/login", controllers.c_users.loginUser) // Login Service
r_user.post("/register", controllers.c_users.insertUser) // Register Service

r_user.put("/profile", controllers.c_users.updateProfile) // Profile Management
r_user.put("/user", controllers.c_users.updateAksesUser) // Admin Management
r_user.put("/user/status", controllers.c_users.updateStatusUser) // Update Status user Banned

module.exports = r_user