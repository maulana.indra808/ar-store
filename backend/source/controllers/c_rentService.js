const jwt = require("jsonwebtoken");
const models = require("../models/index");

let c_rent = {}

c_rent.insertPesanan = async (req, res) =>
{
  // Cek user login
  // TODO: Aktifkan setelah menggunakan session
  // if(!(req.session.email)) { res.status(404).json({pesan: `Anda harus login terlebih dahulu`, status: false}) }
  // else
  // {

    // let email = req.session.email
    let email = req.body.email // TODO: Ubah menggunakan data session
    // Cek status pinjaman user

    await models.m_users.cekStatusPinjaman(email, (err, result) => 
    {
      if(err) { res.status(500).json(err) }

      else
      {
        const status_pinjaman = result[0].status_pinjaman

        // Ketika status user dalam keadaan terlambat
        if(status_pinjaman == "terlambat") 
        { res.status(200).json({pesan: "User memiliki keterlambatan, selesaikan terlebih dahulu sebelum melakukan peminjaman kembali", status:false}) }

        // ketuka status user dalam keadaan meminjam
        else if(status_pinjaman == "meminjam")
        { res.status(200).json({pesan: "User memiliki pinjaman, kembalikan terlebih dahulu pinjaman sebelumnya", status: false}) }

        // ketika status user bebas-pinjaman
        else if(status_pinjaman == "bebas-pinjaman")
        {
          let data = req.body
          const token = jwt.sign({data}, process.env.KEYS, {expiresIn: "1h"}) // generate token
          let field_api = {email, api: token, kategori: "Insert Pesanan", keterangan: "Sukses", createdAt: new Date()}
        
          models.m_rent.InsertPesanan(data, (err, result) => 
          {
            (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Item berhasil ditamhakan kedalam keranjang`, status: true})
          })

          models.m_api.insertAPILog(field_api, (err, result) =>
          {
            if(err){ console.error(err) }
            else {console.log("Log Api Insert Pesanan disimpan")
          }
          })

        //   models.m_users.updateLogin({status_pinjaman : status_pinjaman, email: email}, (err, result) => {
        //     (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Berhasil update status pinjaman`, status: true})
        // })

        }
      }
    })


  // }
}

c_rent.insertFaktur = async (req, res) =>
{
  // validasi pembayaran
  if(!(req.body.nominal_tagihan)) {res.status(406).json({ pesan: `Nominal tagihan tidak boleh kosong`, status: false })}

  else
  {
    // const email = req.session.email
    const email = req.body.email // TODO: Ganti menggunakan session

    const tanggal = new Date()
    const pengembalian = new Date();
    const id_faktur = new Date().getTime()
    pengembalian.setDate((tanggal.getDate()) + 4);

    let data = req.body
    data.tanggal_peminjaman = tanggal
    data.tanggal_pengembalian = pengembalian
    data.id_faktur = id_faktur
    data.isLunas = 0
    delete data.email // TODO : Hapus setelah menggunakan session

    let field_pesanan = { email, id_faktur, status_pesanan: `menunggu pembayaran`}

    const token = jwt.sign({data}, process.env.KEYS, {expiresIn: "1h"}) // generate token
    let field_api = {email, api: token, kategori: "Update Pesanan", keterangan: "Sukses", createdAt: new Date()}

    models.m_rent.insertFaktur(data, (err, result) => 
    {
      (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Faktur pesanan berhasil dibuat`, status: true})
    })

    const update_pesanan = updatePesanan(field_pesanan)

    models.m_api.insertAPILog(field_api, (err, result) =>
    {
      if(err){ console.error(err) }
      else {console.log("Log Api Update Pesanan disimpan")
    }
    })

    // res.json({data})
  }
}

function updatePesanan(params) {
  const data = 
  {
    email: params.email,
    status_pesanan: params.status_pesanan,
    id_faktur: params.id_faktur,
    updatedAt : new Date()

  }

  // console.log(data)
  models.m_rent.updatePesanan(data, (err, result) => 
  {
    let callback
    (err) ? callback = err : callback = {pesan: `Data pesanan berhasil riperbaharui`, status: true}
    console.log (callback)
  })
}

module.exports = c_rent