const jwt = require("jsonwebtoken");
const models = require("../models/index");

let c_review = {}


function insert_api (email, params, kategori)
{
  // const email = req.session.email
  const api = jwt.sign({params}, process.env.KEYS, {expiresIn: "1h"}) // generate token
  const atribut = {kategori, keterangan : "sukses", createdAt : new Date()}

  let field_api = {email, api, kategori : kategori, keterangan : atribut.keterangan, createdAt : atribut.createdAt}
  models.m_api.insertAPILog(field_api, (err, result) =>
  {
    let callback
    (err) ? callback = err : callback = {pesan: "Insert Api", email, atribut}
    console.log(callback)
  })  
}

c_review.insertReview = async (req, res) =>
{
  // const email = req.session.email
  let email = req.body.email // TODO: Ubah menggunakan session

  let data = req.body
  delete data.email

  await models.m_review.insertReview(data, (err, result) =>
  {
    (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Berhasil memberikan review untuk pesanan ${req.body.id_pesanan}`, status: true})
  })

  insert_api(email, data, kategori = "insert Review")

}

c_review.updateReview = async (req, res) => 
{
  let data = req.body

  console.log(data)

  // models.m_review.updateReview(data, (err, result) => 
  // {
  //   if(err) { res.status(200),json(err) } else { res.status(200).json({pesan :`Review dengan id ${data.id_review} berhasil dihapus`, status: true}) }
  // })

  // insert_api(email, data, kategori = "hapus review")
}

c_review.deleteReview = async (req, res) =>
{
  let data = req.body
  // res.json(data)

  models.m_review.updateReview(data, (err, result) => 
  {
    if(err) { res.status(200).json(err) } else { res.status(200).json({pesan :`Review dengan id ${data.id_review} berhasil dihapus`, status: true}) }
  })

  // insert_api(email, data, kategori = "hapus review")
}

module.exports = c_review