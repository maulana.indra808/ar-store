const c_users = require("./c_users")
const c_movies = require("./c_movies")
const c_rent = require("./c_rentService")
const c_transaksi = require("./c_transaksi")
const c_return = require("./c_return")
const c_review = require("./c_review")
const c_alert = require("./c_alert")

const controllers = {
  c_users,
  c_movies,
  c_rent,
  c_transaksi,
  c_return,
  c_review,
  c_alert
}
module.exports = controllers