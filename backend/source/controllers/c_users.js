const session = require("express-session");
const models = require("../models/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

let c_users = {}

// TODO: Belum ada error Handler


c_users.loginUser = async (req, res) => {
  const { email, kata_sandi } = req.body
  await models.m_users.cekEmail(email, (err, result) => {
    const user_data = result.data[0];

    // Validasi form //
    if (!(email && kata_sandi)) { res.status(406).json({ pesan: "Form login harus di isi" }) }

    // Logic login
    else {

      if (result.status == true) {
        res.status(406).json({ pesan: "Login gagal", hit: "Email tidak terdaftar" })
      }

      else if (result.status == false) {

        // Komparasi katasandi
        const d_kata_sandi = bcrypt.compare(kata_sandi, user_data.kata_sandi, (err, sesuai) => {
          // Kata sandi sesuai
          if (sesuai) {
            // akun banned //
            if (user_data.isBanned == true) { res.status(403).json({ pesan: "Akun anda di Banned" }) }

            // Akun aktif (tidak banned)
            else if (user_data.isBanned == false) {
              // Set session login //
              // req.session.user = { akses: user_data.akses, email: user_data.email, status: user_data.status_pinjaman }
              // req.session.email = user_data.email
              // req.session.akses = user_data.akses
              // req.session.status = user_data.status
              // req.session.save()
              // end session

              const email = user_data.email
              const aksses = user_data.akses
              const status = user_data.status

              const aksesToken = jwt.sign({email, aksses, status}, process.env.KEYS,
                {
                  expiresIn: '30s'
                })

              const refreshToken = jwt.sign({email, aksses, status}, process.env.KEYS,
                {
                  expiresIn: '1d'
                })

              models.m_users.updateLogin({refreshToken : refreshToken, email: email}, (err, result) =>
              {
                if(err) res.status(500).json(err)  
              })

              res.cookie('user_token', refreshToken,
              {
                httpOnly: true,
                maxAge: 24 * 60 * 60 * 1000,                
              })


              res.status(200).json({ pesan: "Berhasil login", aksesToken, status: true })
            }
            // end akun aktif
          }

          // Ketika kata sandi tidak sesuai
          else if (!sesuai) { res.status(400).json({ pesan: "Login gagal", clue: "Kata sandi tidak sesuai" }) }

          // response error internal
          else { res.status(500).json(err) }
        })



      }

    } // end else

    if (err) { res.status(500).json(err) }
  })
}

c_users.lihatProfiile = async (req, res) =>
{
  const email = req.body.email
  await models.m_users.lihatProfile(email, (err, result) =>
  {
    if (result == true) { res.status(200).json({ result }) }
  })
}

c_users.lihatSemuaProfiile = async (req, res) =>
{
  await models.m_users.lihatSemuaProfile(null, (err, result) =>
  {
    if(err){ res.status(500).json(err)}
    res.status(200).json(result)    
  })
}

c_users.insertUser = async (req, res) => {
  const { email, kata_sandi, konfirm_kata_sandi } = req.body
  const e_kataSandi = await bcrypt.hash(kata_sandi, 10)

  if (!(email && kata_sandi && konfirm_kata_sandi)) {
    res.status(400).json({ pesan: `Kolom input harus terpenuhi` })
  }

  // konfirmasi katasandi
  else if (kata_sandi != konfirm_kata_sandi) {
    res.status(400).json({ pesan: `Kata sandi tidak cocok`, status: false })
  }


  else // katasandi sesuai -> validasi email
  {
    // Pemeriksaan email sudah terdaftar
    await models.m_users.cekEmail(email, (err, result) => {

      if (err) { res.status(500).json({ err }) }

      // response ketika email sudah terdaftar
      if (result.status == false) {
        res.status(400).json(
          {
            pesan: "Email yang anda masukan tidak dapat didaftarkan lagi",
            validasi: false,
          }
        )
      }

      // insert User
      else {
        let data = { email, kata_sandi }
        data.kata_sandi = e_kataSandi
        data.email = email.toLowerCase()

        const token = jwt.sign({ data }, process.env.KEYS, { expiresIn: "1h" }) // generate token
        let field_api = { email, api: token, kategori: "insert user", keterangan: "Sukses", createdAt: new Date() }
        let field_profile = { email }

        models.m_users.insertLogin(data, (err, result) => // memanggil service insert data login
        {
          if (err) { res.status(500).json({ err }) }

          // response ketika berhasil insert data
          if (result == true) { res.status(200).json({ pesan: `Anda berhasil melakukan pendaftaran, silahkan login` }) }
        })

        models.m_users.insertProfile(field_profile, (err, result) => {
          if (err) { console.log(err) }
          console.log("Data profile behasil dibuat")
        })

        models.m_api.insertAPILog(field_api, (err, result) => {
          if (err) { console.error(err) }
          else {
            console.log("Log Api Insert Movie berhasil disimpan")
          }
        })

      } // end else


    })

  }


}

c_users.updateProfile = async (req, res) => {
  const { email, nama_lengkap, alamat, nomor_telpon } = req.body
  const foto = req.file.path
  let data = req.body
  data.foto = foto

  const token = jwt.sign({ data }, process.env.KEYS, { expiresIn: "1h" }) // generate token
  let field_api = { email, api: token, kategori: "Update Profile", keterangan: "Sukses", createdAt: new Date() }

  if (!(req.session.user)) {
    res.status(406).json({ pesan: "Anda harus login terlebih dahulu" })
  }

  else {
    if (!(email && nama_lengkap && alamat && nomor_telpon)) {
      res.status(406).json({ pesan: "Form biodata tidak boleh dikosongkan" })
    }

    else {
      let data = req.body
      data.foto = foto

      await models.m_users.updateProfile(data, (err, result) => {
        res.json(result)
      })

      models.m_api.insertAPILog(field_api, (err, result) => {
        if (err) { console.error(err) }
        else {
          console.log("Log Api Insert Movie berhasil disimpan")
        }
      })

    }
  }


}

c_users.updateAksesUser = async (req, res) => {
  // if(!(req.session.user)) {res.status(404).json({pesan: "Anda harus login terlebih dahulu"})}
  // else
  // {

  const { email, akses } = req.body
  let data = req.body

  const token = jwt.sign({ data }, process.env.KEYS, { expiresIn: "1h" }) // generate token
  let field_api = { email, api: token, kategori: "Update Akses User", keterangan: "Sukses", createdAt: new Date() }

  if (!(email, akses)) { res.status(406).json({ pesan: "Inputan data tidak sesuai" }) }

  else {
    await models.m_users.updateAksesUser(data, (err, result) => {
      if (err) { res.status(500).json({ status: false, err }) }

      else { res.status(200).json({ status: true, pesan: `Akses untuk email ${email} berhasil dirubah menjadi ${akses}` }) }
    })

    models.m_api.insertAPILog(field_api, (err, result) => {
      if (err) { console.error(err) }
      else {
        console.log("Log Api Insert Movie berhasil disimpan")
      }
    })
  }

  // }
}

c_users.updateStatusUser = async (req, res) => {
  // if(!(req.session.user)) {res.status(404).json({pesan: "Anda harus login terlebih dahulu"})}
  // else
  // {

  const { email, isBanned } = req.body
  let data = req.body

  const token = jwt.sign({ data }, process.env.KEYS, { expiresIn: "1h" }) // generate token
  let field_api = { email, api: token, kategori: "Update Status User", keterangan: "Sukses", createdAt: new Date() }

  if (!(email, isBanned)) { res.status(406).json({ pesan: "Inputan data tidak sesuai" }) }

  else {
    await models.m_users.updateStatusUser(data, (err, result) => {
      if (err) { res.status(500).json({ status: false, err }) }

      else { res.status(200).json({ status: true, pesan: `Status untuk email ${email} berhasil dirubah menjadi ${result.status_user}` }) }
    })

    models.m_api.insertAPILog(field_api, (err, result) => {
      if (err) { console.error(err) }
      else {
        console.log("Log Api Insert Movie berhasil disimpan")
      }
    })
  }

  // }
}

c_users.UpdateStatusPinjaman = async (req, res) => {
  // const email = req.session.email
  const email = req.body.email // TODO : Ganti menggunakan session email
  let data = req.body

  const token = jwt.sign({ data }, process.env.KEYS, { expiresIn: "1h" }) // generate token
  let field_api = { email, api: token, kategori: "Update Status Pinjaman User", keterangan: "Sukses", createdAt: new Date() }

  await models.m_users.cekStatusPinjaman(email, (err, result) => {
    if (err) { res.status(500).json(err) }
    else { res.status(200).json({ status: true, status_pinjaman: result[0].status_pinjaman }) }
  })

  models.m_api.insertAPILog(field_api, (err, result) => {
    if (err) { console.error(err) }
    else {
      console.log("Log Api Insert Movie berhasil disimpan")
    }
  })

}

c_users.cekStatusPinjaman = async (req, res) => {
  const email = req.body.email

  await models.m_users.cekStatusPinjaman(email, (err, result) => {
    if (err) { res.status(500).json(err) }
    else { res.status(200).json({ status: true, status_pinjaman: result[0].status_pinjaman }) }
  })
}

module.exports = c_users