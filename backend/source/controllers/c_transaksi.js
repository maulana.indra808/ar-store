const jwt = require("jsonwebtoken");
const models = require("../models/index");

let c_transaksi = {}
// const email = req.email.session

function insert_api (email, params, kategori)
{
  // const email = req.session.email
  const api = jwt.sign({params}, process.env.KEYS, {expiresIn: "1h"}) // generate token
  const atribut = {kategori, keterangan : "sukses", createdAt : new Date()}

  let field_api = {email, api, kategori : kategori, keterangan : atribut.keterangan, createdAt : atribut.createdAt}
  models.m_api.insertAPILog(field_api, (err, result) =>
  {
    let callback
    (err) ? callback = err : callback = {pesan: "Insert Api", email, atribut}
    console.log(callback)
  })  
}

function update_user (email, data)
{
  let field_updateUser = {email: email, status_pinjaman: data.status_pinjaman}
  models.m_users.updateLogin(field_updateUser, (err, result) => 
  {
    let callback
    if(err) { callback = err } else { callback = `user ${email} berubah menjadi ${data.status_pinjaman}` }
    console.log(callback)
  })
}

function update_status_pesanan (email, data)
{
  let field_updateStatusPesanan = {email: email, id_faktur: data.id_faktur, status_pesanan: data.status_pesanan}
  models.m_rent.updateStatusPesanan(field_updateStatusPesanan, (err, result) => 
  {
    let callback
    if(err) { callback = err } else { callback = `status pesanan id faktur ${data.id_faktur} menjadi ${data.status_pesanan}` }
    console.log(callback)
  })
}

function update_pembayaran(data)
{
  let field_status_pembayaran = {id_faktur : data.id_faktur, isLunas: data.isLunas}
  models.m_rent.updatePembayaran(field_status_pembayaran, (err, result) => 
  {
    let callback
    if(err) { callback = err } else { callback = `status pembayaran id faktur ${data.id_faktur} menjadi ${data.isLunas}` }
    console.log(callback)
  })
}

function update_stok_movie(data)
{
  let id_faktur = data.id_faktur
  models.m_rent.cariPesanan({field: ['id_movie', 'id_faktur'], where: "id_faktur", key: id_faktur}, (err, result) => 
  {
    if(result)
    {
      // Lihat data movie yang dipesan -> update sok movie
      models.m_rent.joinPesananMovie({key: id_faktur}, (err, result) =>
      {
        if(result)
        {
          let urutan = 0
          let data = result.result

            // update stok movie
            data.forEach(element => {
            let stok = data[urutan].jumlah_stok - 1

            models.m_movies.updateStok({id_movie: data[urutan].id_movie, jumlah_stok: stok}, (err, result) => 
            {
              if(result) { console.log(`Berhasil Update Stok movie`) } else {console.log(err)}
            })

            urutan++
          });

        }

        // join Pesanan Movie
        else{console.log(err)}
      })
    }

    // cariPesanan
    else {console.log(err)}
  })

  // models.m_movies.cariMovie({field: ['id_movie', 'jumlah_stok'], where: 'id_movie', key: 1}, (err, result) => 
  // {
  //   let {id_movie, jumlah_stok} = result.data
  //   console.log(stok_movie)
  // })
}

c_transaksi.insertTransaksi = async (req, res) =>
{
  // const email = req.email.session
  let email = req.body.email // TODO Ganti dengan session
  let data = req.body
  delete data.email // TODO: hapus

  // Insert transaksi
  await models.m_transaksi.insertTransaksi(data, (err, result) =>
  {
    (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Data transaksi berhasil disimpan`, status: true})
  })

  update_user(email, {status_pinjaman : "meminjam"})
  update_status_pesanan(email, {id_faktur: req.body.id_faktur, status_pesanan: "menunggu pengembalian"})
  update_pembayaran({id_faktur: req.body.id_faktur, isLunas: 1})
  update_stok_movie({id_faktur: req.body.id_faktur})
  insert_api(email, data, kategori = "insert transaksi")

  // console.log(req.body)
}

module.exports = c_transaksi