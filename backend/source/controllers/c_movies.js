const jwt = require("jsonwebtoken");
const models = require("../models/index");

let c_movies = {}

c_movies.insertMovie = async (req, res) => 
{
  const {judul, kategori, kualitas, sinopsis, sutradara, jam_tayang, jumlah_stok, status, harga} = req.body
  let email = req.body.email // TODO : ganti menjadi session
  let data = req.body
  delete data.email

  // Validasi empty kolom
  if(!(judul && kategori && kualitas && sinopsis && sutradara && jam_tayang && jumlah_stok && status && harga))
  {
    res.status(406).json({pesan: `Form tambah data movie tidak boleh dikosongkan`, status: false})
  }

  // proses input data movie
  else 
  {
    const token = jwt.sign({data}, process.env.KEYS, {expiresIn: "1h"}) // generate token
    let field_api = {email, api: token, kategori: "insert data movie", keterangan: "Sukses", createdAt: new Date()}

    await models.m_movies.insertMovie(data, (err, resull) => 
    {
      (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Data Movie berhasil disimpan`, status: true})
    })

    models.m_api.insertAPILog(field_api, (err, result) =>
    {
      if(err){ console.error(err) }
      else {console.log("Log Api Insert Movie berhasil disimpan")
    }
    })

  }

}

c_movies.updateMovie = async (req, res) => 
{
  let email = req.body.email // TODO : Ganti dengan session
  let data = req.body
  delete data.email // TODO: Hapu setelah menggunakan session

  const token = jwt.sign({data}, process.env.KEYS, {expiresIn: "1h"}) // generate token
  let field_api = {email, api: token, kategori: "Update data movie", keterangan: "Sukses", createdAt: new Date()}

  await models.m_movies.updateMovie(data, (err, resull) => 
  {
    (err) ? res.status(500).json(err) : res.status(200).json({pesan: `Data Movie berhasil diperbarui`, status: true})
  })

  models.m_api.insertAPILog(field_api, (err, result) =>
  {
    if(err){ console.error(err) }
    else {console.log("Log Api Insert Movie berhasil disimpan")
  }
  })

}

module.exports = c_movies