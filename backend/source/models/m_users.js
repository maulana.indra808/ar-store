const bcrypt = require("bcrypt")
const db = require("../config/database/connection")
let m_users = {}

m_users.cekEmail = async (req, callback) =>
{
  const email = req
  const query = `SELECT * FROM login WHERE email = ?`
  await db.query(query, email, (err, result) => 
    {
      if(err) throw callback(err, null)

      if(result == ""){
        callback(null, {status:true})
      } else {
        callback(null, {status: false, data: result})
      }
    }
  )
}

m_users.lihatProfile = async (req, callback) =>
{
  let email = req
  const query = `SELECT * FROM profile WHERE email = ${email}`
  await db.query(query, null, (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, result)
  })
}

m_users.lihatSemuaProfile = async (req, callback) =>
{
  const query = `SELECT * FROM profile`
  await db.query(query, (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, result)
  })
}

m_users.insertLogin = async (req, callback) =>
{
  let data = req
  data.akses = "user"
  data.status_pinjaman = "bebas pinjaman"
  data.isBanned = false
  data.createdAt = new Date()


  
    const query = `INSERT INTO login SET ?`
    await db.query(query, data, (err, result) =>
      {
        if(err) callback({status: false, err}, null)
        callback(null, true)
      }
    )
}

m_users.insertProfile = async (req, callback) =>
{
  let data = req
  data.createdAt = new Date()
  
    const query = `INSERT INTO profile SET ?`
    await db.query(query, data, (err, result) =>
      {
        if(err) callback({status: false, err}, null)
        callback(null, true)
      }
    )
}

m_users.updateProfile = async (req, callback) =>
{
  const email = req.email
  let data = req
  data.updatedAt = new Date()
  delete data.email

  // console.log(data)
  // console.log(email)

  const query = `UPDATE profile SET ? WHERE email = ?`
  await db.query(query, [data, email], (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_users.updateAksesUser = async (req, callback) =>
{
  const email = req.email
  let data = req
  data.updatedAt = new Date()
  delete data.email

  const query = `UPDATE login SET ? WHERE email = ?`
  await db.query(query, [data, email], (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })

}

m_users.updateStatusUser = async (req, callback) =>
{

  const email = req.email
  
  let statusUser = ''
  let data = req
  data.updatedAt = new Date()
  delete data.email

  if(data.isBanned == 0) {statusUser = `normal`}
  else {statusUser = 'Banned'}

  const query = `UPDATE login SET ? WHERE email = ?`
  await db.query(query, [data, email], (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, {status: true, status_user: statusUser})
  })

}

m_users.updateLogin = async (req, callback) =>
{
  const email = req.email
  let data = req
  data.updatedAt = new Date()
  delete data.email

  const query = `UPDATE login SET ? WHERE email = ?`
  await db.query(query, [data, email], (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_users.cekStatusPinjaman = async (req, callback) => 
{
  const email = req

  const query = `SELECT * from login WHERE email = ?`
  await db.query(query, email, (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, result)
  })
}

module.exports = m_users