const db = require("../config/database/connection")
let m_review = {}

m_review.insertReview = async (req, callback) =>
{
  const data = req
  data.createdAt = new Date()

  const query = `INSERT INTO review SET ?`
  await db.query(query, data, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_review.updateReview = async (req, callback) =>
{
  let id_review = req.id_review
  let data = req
  data.updatedAt = new Date()
  delete data.id_review

  const query = `UPDATE review SET ? WHERE id_review = ?`
  await db.query(query, [data, id_review], (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

module.exports = m_review