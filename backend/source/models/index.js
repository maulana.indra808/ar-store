const m_users = require("./m_users")
const m_api = require("./m_api")
const m_movies = require("./m_movies")
const m_rent = require("./m_rentService")
const m_transaksi = require("./m_transaksi")
const m_review = require("./m_review")

const models = {
  m_users,
  m_api,
  m_movies,
  m_rent,
  m_transaksi,
  m_review,

}

module.exports = models