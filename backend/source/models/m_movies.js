const db = require("../config/database/connection")

let m_movies = {}

m_movies.cariMovie = async (req, callback) =>
{
  const field = req.field
  const where = req.where
  const key = req.key

  const query = `SELECT ${field} FROM movie WHERE ${where} = ${key}`
  await db.query(query, null, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, data: result})
  })
}

m_movies.updateStok = async (req, callback) =>
{
  const {id_movie, jumlah_stok} = req

  const query = `UPDATE movie SET jumlah_stok = ${jumlah_stok} WHERE id_movie = ${id_movie}`
  await db.query(query, null, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_movies.insertMovie = async (req, callback) =>
{
  let data = req
  data.createdAt = new Date()

  const query = `INSERT INTO movie SET ?`
  await db.query(query, data, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_movies.updateMovie = async (req, callback) =>
{
  let id = req.id

  let data = req
  data.updatedAt = new Date()
  delete data.id

  const query = `UPDATE movie SET ? WHERE id_movie = ?`
  await db.query(query, [data, id], (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })

}

module.exports = m_movies