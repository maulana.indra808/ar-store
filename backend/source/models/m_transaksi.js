const db = require("../config/database/connection")
let m_transaksi = {}

m_transaksi.insertTransaksi = async (req, callback) =>
{
  let data = req
  data.createdAt = new Date()

  const query = `INSERT INTO transaksi SET ?`
  await db.query(query, data, (err, result) =>
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

module.exports = m_transaksi