const db = require("../config/database/connection")

let m_api = {}

m_api.insertAPILog = async (req, callback) => {
  const data = req

  const query = `INSERT INTO api_log set ?`
  await db.query(query, data, (err, result) => 
    {
      (err) ? callback(err, null) : callback(null, true)
    }
  )
}

module.exports = m_api