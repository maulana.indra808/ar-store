const db = require("../config/database/connection")
let m_rent = {}

m_rent.cariPesanan = async (req, callback) =>
{
  const field = req.field
  const where = req.where
  const key = req.key

  const query = `SELECT ${field} FROM pesanan WHERE ${where} = ${key}`
  await db.query(query, null, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_rent.InsertPesanan = async (req, callback) =>
{
  let data = req
  data.createdAt = new Date()

  const query = `INSERT INTO pesanan SET ?`
  await db.query(query, data, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_rent.insertFaktur = async (req, callback) =>
{
  let data = req
  data.createdAt = new Date()

  const query = `INSERT INTO faktur SET ?`
  await db.query(query, data, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_rent.updatePesanan = async (req, callback) => // merubah id_faktur
{
  let email = req.email
  let status_pesanan = req.status_pesanan

  let data = req
  delete data.email
  delete data.status_pesanan

  data.updatedAt = new Date()

  const query = `UPDATE pesanan SET ? where email = ? AND status_pesanan = ?`
  await db.query(query, [data, email, status_pesanan], (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_rent.updateStatusPesanan = async (req, callback) => // merubah status pemesanan
{
  let email = req.email
  let id_faktur = req.id_faktur

  let data = req
  delete data.email
  delete data.id_faktur

  data.updatedAt = new Date()

  const query = `UPDATE pesanan SET ? where email = ? AND id_faktur = ?`
  await db.query(query, [data, email, id_faktur], (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_rent.updatePembayaran = async (req, callback) =>
{
  let id_faktur = req.id_faktur
  
  let data = req
  data.updatedAt = new Date()
  delete data.id_faktur

  const query = `UPDATE faktur SET ? WHERE id_faktur = ?`
  await db.query(query, [data, id_faktur], (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}

m_rent.joinPesananMovie = async (req, callback) =>
{
  let key = req.key
  
  const query = `SELECT pes.email, pes.id_faktur, pes.status_pesanan, mov.id_movie, mov.jumlah_stok 
  FROM pesanan pes 
  JOIN movie mov 
  ON mov.id_movie = pes.id_movie 
  WHERE pes.id_faktur = ${key}`

  await db.query(query, null, (err, result) => 
  {
    (err) ? callback(err, null) : callback(null, {status: true, result})
  })
}
module.exports = m_rent