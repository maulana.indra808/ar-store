const dotenv = require("dotenv");
const mysql = require("mysql");


const env = dotenv.config()
let connections

try {
  connections = mysql.createConnection(
    {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME
    }
  )
} catch (error) {
  console.error(error)
  
} finally {
  connections.connect((err, hasil) => {
    (err) ? console.error(err) : console.info(`Connected with db:${process.env.DB_NAME}`)
  })
}

module.exports = connections