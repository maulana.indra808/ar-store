const dotenv = require("dotenv");
const env =  dotenv.config()
const jwt = require("jsonwebtoken")

let verify_token = {}

verify_token = (req, res, next) =>
{
  const autHeader = req.headers['authorization'];
  const token = autHeader && autHeader.split(' ');

  if(token == null) return res.sendStatus(401)
  jwt.verify(token, process.env.KEYS, (err, decode) =>
  {
    if(err) return res.sendStatus(403)
    req.email = decode.email
    next()
  })
}

module.exports = verify_token