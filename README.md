Selamat Siang.
Saya infokan untuk dokumentasi sementara bisa dicek melalui tautan dibawah ini:
https://docs.google.com/spreadsheets/d/1GUfh_XleBpqKdFKkG7j7j5obaufSY7Qg/edit?usp=sharing&ouid=110918366342524319874&rtpof=true&sd=true

## Installasi
1. Buka terminal
2. masuk ke folder dimana repositori project ini di clone
3. lalu jalankan perintah npm install --save
4. lalu jalankan perinta npm start

## NOTE
1. Project ini saya buat menggunakan operating system unix
2. Database yang digunakan adalah mysql (arstore)
3. Host yang digunakan http://127.0.0.1:8027/ untuk backand dan
4. host untuk front end ada di http://127.0.0.1:8888 (comingsoon)
5. Mohon maaf untuk segala kekuranganya

## License
For open source projects, say how it is licensed.